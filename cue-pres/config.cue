repos: [...#Repo]

#Repo: {
	name:    string | !=""
	enabled: bool | *true
	url:     string
	files:   number & >=1
	min:     number | *0
	max?:    number & min > max
}

repos: {
	[
		{name: "Test 1", url: "https://www.example.com/test1", files: 5},
		{name: "Test 2", url: "https://www.example.com/test2", files: 9},
		{name: "Test 3", url: "https://www.example.com/test3", files: 0, enabled: false},
		{name: "Test 4", url: "https://www.example.com/test4", files: 2},
	]
}
