It's A Live Presentations
=========================

These are a few presentations/"live" demos I've worked on.
They run on [It's A Live](https://pypi.org/project/itsalive/).

To run them, clone the repo, and, well, good luck.
